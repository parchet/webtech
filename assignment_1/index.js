
const FIRST_NAME = "Alex";
const LAST_NAME = "Stoian";
const GRUPA = "1083";

/**
 * Make the implementation here
 */
function numberParser(input) {
    if (input !== input)
    	return NaN;
    if (typeof input === 'number' && Number.isFinite(input) === false)
    	return NaN;
    if (input>Number.MAX_SAFE_INTEGER || input<Number.MIN_SAFE_INTEGER)
    	return NaN;
    return parseInt(input)
}

module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    numberParser
}

