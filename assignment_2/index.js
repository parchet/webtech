
const FIRST_NAME = "Alex";
const LAST_NAME = "Stoian";
const GRUPA = "1083";

function initCaching() {
   var obj1 = {
   	pageAccessCounter(x){
   		if (typeof(x)==='undefined')
   			x = 'home';
   		else x = x.toLowerCase();

   		if (this[x] > 0)
   			this[x]++;
   		else this[x] = 1;
   	}, 
   getCache(){
   	return this;
   }
   };

   return obj1;
}


module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    initCaching
}

