
const FIRST_NAME = "Alex";
const LAST_NAME = "Stoian";
const GRUPA = "1083";


class Employee {
   constructor(name, surname, salary){
   	this.name = name;
   	this.surname = surname;
   	this.salary = salary;
   }
   getDetails(){
   	return this.name + " " + this.surname + " " + this.salary;
   }
}

class SoftwareEngineer extends Employee{
   constructor(name, surname, salary, exp){
   	super(name, surname, salary);
   	if (exp)
   	this.experience = exp;
    else this.experience = 'JUNIOR';
   }

   applyBonus(){
   	if (this.experience === 'MIDDLE')
   		return this.salary*1.15;
   	else if (this.experience === 'SENIOR')
   		return this.salary*1.2;
   	else return this.salary*1.1;
   }
}

module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    Employee,
    SoftwareEngineer
}

